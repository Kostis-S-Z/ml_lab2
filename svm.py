#!/usr/bin/env python
# -- coding: utf-8 --

# import packages
from scipy . optimize import minimize
import matplotlib . pyplot as plt
import numpy as np
import random, math


# variables
C = None
train_data = []
labels = []
pre_mat = []


A1points = 10 # 2  # 50
A2points = 10
B1points = 10  # 50  # 50
B2points = 10
N = A1points + A2points + B1points + B2points # the number of training samples (each training sample has a corresponding α-value).

"""
upperAboundX = 1.5
lowerAboundX = -1.5
upperAboundY = 0.5
lowerAboundY = 0.5

upperBboundX = 0.0
lowerBboundX = 0.0
upperBboundY = -0.5
lowerBboundY = -0.5
"""

""" XOR
upperAboundX = 1.5
lowerAboundX = -1.5
upperAboundY = 1.0
lowerAboundY = -1.0

upperBboundX = 1.5
lowerBboundX = -1.5
upperBboundY = -1.
lowerBboundY = 1.0
"""

""" overlapping """
upperAboundX = 1.5
lowerAboundX = -1.5
upperAboundY = 0.5
lowerAboundY = 0.5

upperBboundX = upperAboundX
lowerBboundX = lowerAboundX
upperBboundY = upperAboundY
lowerBboundY = lowerAboundY
""" """

def kernel(x, y):
    #return linear(x, y)
    return poly(x, y, 5)
    #return rbf(x, y, 1)

def linear(x, y):
    return np.dot(x,y)

def poly(x, y, p):
    return (np.dot(x,y)+1)**p

def rbf(x, y, sigma):
    return np.exp(np.sqrt(np.dot(x-y,x-y)**2)/ (2*sigma**2))

def pre_com():
    pre = np.zeros((N, N))
    for i in range(N):
        for j in range(N):
            pre[i][j] = labels[i] * labels[j] * kernel(train_data[i], train_data[j])
    return pre

def objective(a):
    """
    Equation 4
    takes a vector a as argument and returns a scalar value
    effectively implementing the expression that should be minimized
    """

    a_t = np.transpose(a)
    part = np.dot(a_t, np.dot(pre_mat, a))
    part2 = np.sum(a)

    result = 0.5 * part - part2

    return result

def indicator(a, test_data, use, b):
    """
    Dual Formulation
    Equation 6
    """
    sum = 0

    for i in use[0]:
        sum += ( a[i] * labels[i] * kernel(test_data, train_data[i]) ) - b

    return sum  # sum should be [-1, 1, 0]

def calc_b(a, s):
    """
    Equation 7
    """
    b = 0

    #for k in range(len(s)):
    for i in range(len(s)):
        b += ( a[i] * labels[i] * kernel(s[0], train_data[i]) ) - t_s[0]

    return b

def zerofun(a):
    """
    Equation 10
    """
    return np.dot(a, labels)


def data():
    """
    Generate random data
    """
    # if you want the same "random" data each time use
    np.random.seed(14)
    # classA = np.random.randn(A1points, 2) * 0.2 + [upperAboundX, upperAboundY]
    classA = np.concatenate(
     (np.random.randn(A1points, 2) * 0.2 + [upperAboundX, upperAboundY],
     np.random.randn(A2points, 2) * 0.2 + [lowerAboundX, lowerAboundY]))

    #classB = np.random.randn(Bpoints, 2) * 0.2 + [upperBboundX, upperBboundY]
    classB = np.concatenate(
     (np.random.randn(B1points, 2) * 0.2 + [upperBboundX, upperBboundY],
     np.random.randn(B2points, 2) * 0.2 + [lowerBboundX, lowerBboundY]))

    inputs = np.concatenate((classA, classB))
    targets = np.concatenate(
     (np.ones(classA.shape[0]),
     -np.ones(classB.shape[0])))

    N = inputs.shape[0]  # Number of rows (samples)

    permute = list(range(N))
    # random.shuffle(permute)
    inputs = inputs[permute, :]
    targets = targets[permute]
    #print(inputs)
    return classA, classB, inputs, targets


def plot_data(classA, classB):
    plt.plot([p[0] for p in classA],
          [p[1] for p in classA],
          'b.')

    plt.plot([p[0] for p in classB],
          [p[1] for p in classB],
          'r.')

    plt.axis('equal')
    plt.savefig('dataplot.pdf')
    plt.show()

def plot_svm(a, use, b):
    plt.plot([p[0] for p in classA],
          [p[1] for p in classA],
          'b.')

    plt.plot([p[0] for p in classB],
          [p[1] for p in classB],
          'r.')

    xgrid = np.linspace(-5, 5)
    ygrid = np.linspace(-4, 4)

    grid = np.array([[indicator(a, [x,y], use, b) for x in xgrid] for y in ygrid])

    plt.contour(xgrid, ygrid, grid,
             (-1.0, 0.0, 1.0),
             colors=('red', 'black', 'blue'),
             linewidths=(1, 3, 1))

    plt.axis('equal')
    plt.savefig('svmplot.pdf')
    plt.show()


# create random data with targets
classA, classB, train_data, labels = data()

plot_data(classA, classB)

pre_mat = pre_com()

# initial guess
start = np.zeros((N, 1))

# B is a list of pairs of the same length as the ⃗α-vector, stating the lower
# and upper bounds for the corresponding element in ⃗α.
# To constrain the α values to be in the range 0 ≤ α ≤ C,
B = [(0, C) for b in range(N)]
XC = {'type':'eq', 'fun':zerofun}
# This will find the vector ⃗α which minimizes the function objective
# within the bounds B and the constraints XC.
ret = minimize ( objective , start , bounds=B, constraints=XC )

alpha = ret ['x']  # There are other useful indices that you can use;

s_v_indeces = np.where( 0.00001 < alpha)

s = train_data[s_v_indeces]  # support vectors
t_s = labels[s_v_indeces]  # labels of support vectors

b = calc_b(alpha, s)

test_data = [
  [-1.5, 0.5], # class A
  [-1.5, 0.0], # class A
  [-1.0, 1.0], # class A
  [0.0, -1.0], # class B
  [0.0, 0.0],  # class B
  [0.0, -0.5],  # class B
  [1.5, -0.5], # ?
  [0.0, 0.5],  # ?
]

# plot_svm(alpha, s_v_indeces, b)

for i in test_data:
    print(indicator(alpha, i, s_v_indeces, b))

# in particular, the index 'success' holds a boolean value which is
# True if the optimizer actually found a solution.

"""
XC is used to impose other constraints, in addition to the bounds. We will
use this to impose the equality constraint, that is, the second half of (10).
This parameter is given as a dictionary with the fields type and fun, stating
the type of the constraint and its implementation, respectively. You can write
an equality constraint like this: constraint={'type':'eq', 'fun':zerofun},
where zerofun is a function you have defined which calculates the value which
should be constrained to zero. Like objective, zerofun takes a vector as
argument and returns a scalar value.
"""
