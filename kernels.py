#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np

class Kernels:

    def linear(x, y):
        return np.dot(x,y)

    def poly(x, y, p):
        return (np.dot(x,y)+1)^p

    def rbf(x, y, sigma):
        return np.exp(np.sqrt(np.dot(x-y,x-y)^2)/ (2*sigma^2))
